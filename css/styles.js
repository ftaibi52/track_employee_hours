import {StyleSheet} from 'react-native';

const options = StyleSheet.create({
  container: {
    //  backgroundColor: '#FF0000',
    padding: 5,
    borderRadius: 5,
    width: 200,
    alignItems: 'center',
  },
  text: {
    fontSize: 25,
    // color: '#FFF',
    marginLeft: 7,
  },
  toucbable: {
    margin: 5,
    borderRadius: 80,
    padding: 40,
    backgroundColor: '#DDDD4D',
  },
  toucbableStop: {
    margin: 5,
    borderRadius: 80,
    padding: 40,
    backgroundColor: '#EF5427',
    
  },
  toucbableLunch: {
    margin: 5,
    borderRadius: 80,
    padding: 40,
    backgroundColor: '#99CD57',
  
  },

  disabled: {
    margin: 5,
    borderRadius: 80,
    padding: 40,
    backgroundColor: '#d6d6d6',
  },
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
  },
  input: {
    width: 300,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: 'black',
    marginBottom: 10,
    borderRadius: 30,
  },

  nombre: {
    margin: 20,
    alignSelf: 'flex-start',
    fontWeight: 'bold',
    textAlignVertical: 'center',
    fontSize: 20,
  },
  form: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
    marginBottom: 20,
  },

  titulo: {
    fontSize: 20,
    marginBottom: 10,
  },

  logo: {
    width: 200,
    height: 200,
    marginBottom: 10,
  },
  list: {
    padding: 10,
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    marginBottom: 10,
    fontSize: 18,
    height: 44,
  },

  textContainer: {
    flexDirection: 'row',
    height: 30,
    margin: 10,
    borderBottomColor: 'black',
    borderBottomWidth: 1,
  },
  leftContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  rightContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  rowLabelText: {
    color: '#0B1219',
    fontSize: 16.0,
  },
  noReservas: {
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  containerMap: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  overlay: {
    position: 'absolute',
    bottom: 50,
    backgroundColor: 'rgba(255, 255, 255, 1)',
  },
  switch: {
    flexDirection: 'row',
    alignSelf: 'flex-start',
    marginLeft: 30,
    marginBottom: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottom: {
    alignItems: 'center',
   // justifyContent: 'center',
    position: 'absolute',
   // alignSelf: 'center',
    bottom: 10,
    flexDirection: 'row',
    
    
  },

});

export default {styles, options};
