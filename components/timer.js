/*This is an Example of Timer/Stopwatch in React Native */
import React, {Component} from 'react';
//import React in our project
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  AsyncStorage,
  Alert,
  ActivityIndicator,
} from 'react-native';
//import all the required components
import {Stopwatch} from 'react-native-stopwatch-timer';
//importing library to use Stopwatch and Timer
import Styles from '../css/styles';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {URL} from '../router/router.js';
import Geolocation from 'react-native-geolocation-service';
import {PermissionsAndroid} from 'react-native';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
let globalTime;
export default class TestApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isStopwatchStart: false,
      isStopwatchStart2: false,

      resetStopwatch: false,
      resetStopwatch2: false,

      empleado_id: '',
      cliente_id: '',
      proyecto_id: '',
      fecha: '',
      hora: '',
      tipo_fichaje: '',
      tiempoAcumulado: '',
      observaciones: '',
      longitud: '',
      latitud: '',
      isActive: '',

      isLoading: false,

      buttonPlay: false,
      buttonPause: true,
      buttonStop: true,
    };

    this.resetStopwatch = this.resetStopwatch.bind(this);
  }

  async componentDidMount() {
    try {
      let empleado_id = await AsyncStorage.getItem('empleado_id', () =>
        console.log('getItem empleado_id completed'),
      );

      let isStopwatchStart = await AsyncStorage.getItem(
        'isStopwatchStart',
        () => console.log('getItem isStopwatchStart completed'),
      );
      let isStopwatchStart2 = await AsyncStorage.getItem(
        'isStopwatchStart2',
        () => console.log('getItem isStopwatchStart2 completed'),
      );
      let resetStopwatch = await AsyncStorage.getItem('resetStopwatch', () =>
        console.log('getItem resetStopwatch completed'),
      );
      let resetStopwatch2 = await AsyncStorage.getItem('resetStopwatch2', () =>
        console.log('getItem resetStopwatch2 completed'),
      );
      let buttonPlay = await AsyncStorage.getItem('buttonPlay', () =>
        console.log('getItem buttonPlay completed'),
      );
      let buttonPause = await AsyncStorage.getItem('buttonPause', () =>
        console.log('getItem buttonPause completed'),
      );
      let buttonStop = await AsyncStorage.getItem('buttonStop', () =>
        console.log('getItem buttonStop completed'),
      );

      console.log(
        'items: ',
        isStopwatchStart,
        isStopwatchStart2,
        resetStopwatch,
        resetStopwatch2,
        buttonPlay,
        buttonPause,
        buttonStop,
      );

      this.setState({
        isStopwatchStart: JSON.parse(isStopwatchStart),
        resetStopwatch: JSON.parse(resetStopwatch),
        isStopwatchStart2: JSON.parse(isStopwatchStart2),
        resetStopwatch2: JSON.parse(resetStopwatch2),
        buttonPlay: JSON.parse(buttonPlay),
        buttonPause: JSON.parse(buttonPause),
        buttonStop: JSON.parse(buttonStop),
      });

      if (empleado_id !== null) {
        // We have data!!
        this.setState({empleado_id: empleado_id});
        console.log('From localStorage empleado_id:', empleado_id);
      } else {
        console.log('No hay resultado en localStrage empleado_id');
      }
    } catch (error) {
      // Error retrieving data
      console.log('Error getItem empleado_id localStorage:', error);
    }
  }

  async resetStopwatch() {
    this.setState({
      isStopwatchStart: false,
      resetStopwatch: true,
      isStopwatchStart2: false,
      resetStopwatch2: true,
      buttonPlay: false,
      buttonPause: true,
      buttonStop: true,
    });

    try {
      await AsyncStorage.setItem(
        'isStopwatchStart',
        this.state.isStopwatchStart.toString(),
        () => console.log('setItem isStopwatchStart completed'),
      );
      await AsyncStorage.setItem(
        'resetStopwatch',
        this.state.resetStopwatch.toString(),
        () => console.log('setItem resetStopwatch completed'),
      );
      await AsyncStorage.setItem(
        'isStopwatchStart2',
        this.state.isStopwatchStart2.toString(),
        () => console.log('setItem isStopwatchStart2 completed'),
      );
      await AsyncStorage.setItem(
        'resetStopwatch2',
        this.state.resetStopwatch2.toString(),
        () => console.log('setItem resetStopwatch2 completed'),
      );

      await AsyncStorage.setItem(
        'buttonPlay',
        this.state.buttonPlay.toString(),
        () => console.log('setItem buttonPlay completed'),
      );
      await AsyncStorage.setItem(
        'buttonPause',
        this.state.buttonPause.toString(),
        () => console.log('setItem buttonPause completed'),
      );
      await AsyncStorage.setItem(
        'buttonStop',
        this.state.buttonStop.toString(),
        () => console.log('setItem buttonStop completed'),
      );
    } catch (error) {
      console.log('Error setitems local storage ', error);
    }
  }
  getFormattedTime(time) {
    console.log('Tiempo: ', time);
    globalTime = time;
    //    this.currentTime = time;
  }
  getYearMonthDay() {
    var dateObj = new Date();
    var month = dateObj.getUTCMonth() + 1;
    var day = dateObj.getUTCDate();
    var year = dateObj.getUTCFullYear();
    var newdate = year + '-' + month + '-' + day;

    return newdate;
  }
  getHour() {
    var horayminuto = new Date();
    var hora = horayminuto.getHours();
    var minuto = horayminuto.getMinutes();
    var horaFinal = hora + ':' + minuto;

    return horaFinal;
  }

  async fichar() {
    this.setState({isLoading: true});

    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      Geolocation.getCurrentPosition(position => {
        this.setState({
          latitud: position.coords.latitude,
          longitud: position.coords.longitude,
        });
      });

      let lastTipoFichaje;
      fetch(URL + 'lastUltimoFichaje?id=' + this.state.empleado_id, {
        method: 'GET',
        //body: {id: this.state.empleado_id},
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
      }).then(async res => {
        let response = await res.json();

        lastTipoFichaje = response.tipo_fichaje;
        console.log('lastTipoFichaje: ', lastTipoFichaje);

        if (lastTipoFichaje == 'Fin de Jornada' || lastTipoFichaje == null) {
          // empieza la jornada
          this.setState({
            tipo_fichaje: 'Inicio de Jornada',
          });

          console.log(' ', lastTipoFichaje, this.state.tipo_fichaje);
        } else {
          this.setState({
            tipo_fichaje: 'Fin de Pausa',
            tiempoAcumulado: globalTime,
          });
          console.log('tiempoAcumulado: ', this.state.tiempoAcumulado);
        }

        let data = {
          empleado_id: this.state.empleado_id,
          cliente_id: this.state.cliente_id,
          proyecto_id: this.state.proyecto_id,
          fecha: this.getYearMonthDay(),
          hora: this.getHour(),
          tipo_fichaje: this.state.tipo_fichaje,
          tiempoAcumulado: this.state.tiempoAcumulado,
          observaciones: this.state.observaciones,
          longitud: this.state.longitud,
          latitud: this.state.latitud,
          isActive: this.state.isActive,
        };

        fetch(URL + 'insertFichaje', {
          method: 'POST',
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest',
          },
        }).then(async res => {
          let response = await res.json();
          if (response.success) {
            // Alert.alert(response.data.tipo_fichaje);
            this.setState({
              isLoading: false,
              isStopwatchStart: true,
              resetStopwatch: false,
              isStopwatchStart2: false,

              buttonPlay: true,
              buttonPause: false,
              buttonStop: false,
            });

            try {
              await AsyncStorage.setItem(
                'isStopwatchStart',
                this.state.isStopwatchStart.toString(),
                () => console.log('setItem isStopwatchStart completed'),
              );
              await AsyncStorage.setItem(
                'resetStopwatch',
                this.state.resetStopwatch.toString(),
                () => console.log('setItem resetStopwatch completed'),
              );
              await AsyncStorage.setItem(
                'isStopwatchStart2',
                this.state.isStopwatchStart2.toString(),
                () => console.log('setItem isStopwatchStart2 completed'),
              );
              await AsyncStorage.setItem(
                'resetStopwatch2',
                this.state.resetStopwatch2.toString(),
                () => console.log('setItem resetStopwatch2 completed'),
              );

              await AsyncStorage.setItem(
                'buttonPlay',
                this.state.buttonPlay.toString(),
                () => console.log('setItem buttonPlay completed'),
              );
              await AsyncStorage.setItem(
                'buttonPause',
                this.state.buttonPause.toString(),
                () => console.log('setItem buttonPause completed'),
              );
              await AsyncStorage.setItem(
                'buttonStop',
                this.state.buttonStop.toString(),
                () => console.log('setItem buttonStop completed'),
              );
            } catch (error) {
              console.log('Error setitems local storage ', error);
            }
          } else {
            this.setState({isLoading: false});
            Alert.alert('Error al fichar');
          }
        });

        console.log('                                   ');
      });
    } else {
      this.setState({isLoading: false});
      // Alert.alert('No tenemeos acceso a tu localización.');
      console.log(' Location permission not granted.');
    }
  }

  async pausa() {
    this.setState({isLoading: true});
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      Geolocation.getCurrentPosition(position => {
        this.setState({
          latitud: position.coords.latitude,
          longitud: position.coords.longitude,
        });
      });

      let data = {
        empleado_id: this.state.empleado_id,
        cliente_id: this.state.cliente_id,
        proyecto_id: this.state.proyecto_id,
        fecha: this.getYearMonthDay(),
        hora: this.getHour(),
        tipo_fichaje: 'Inicio de Pausa',
        tiempoAcumulado: this.getFormattedTime(),
        observaciones: this.state.observaciones,
        longitud: this.state.longitud,
        latitud: this.state.latitud,
        isActive: this.state.isActive,
      };

      fetch(URL + 'insertFichaje', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
      }).then(async res => {
        let response = await res.json();
        if (response.success) {
          // Alert.alert(response.data.tipo_fichaje);

          this.setState({
            isLoading: false,
            isStopwatchStart: false,
            resetStopwatch: false,
            resetStopwatch2: false,
            isStopwatchStart2: true,

            buttonPlay: false,
            buttonPause: true,
            buttonStop: true,
          });

          try {
            await AsyncStorage.setItem(
              'isStopwatchStart',
              this.state.isStopwatchStart.toString(),
              () => console.log('setItem isStopwatchStart completed'),
            );
            await AsyncStorage.setItem(
              'resetStopwatch',
              this.state.resetStopwatch.toString(),
              () => console.log('setItem resetStopwatch completed'),
            );
            await AsyncStorage.setItem(
              'isStopwatchStart2',
              this.state.isStopwatchStart2.toString(),
              () => console.log('setItem isStopwatchStart2 completed'),
            );
            await AsyncStorage.setItem(
              'resetStopwatch2',
              this.state.resetStopwatch2.toString(),
              () => console.log('setItem resetStopwatch2 completed'),
            );

            await AsyncStorage.setItem(
              'buttonPlay',
              this.state.buttonPlay.toString(),
              () => console.log('setItem buttonPlay completed'),
            );
            await AsyncStorage.setItem(
              'buttonPause',
              this.state.buttonPause.toString(),
              () => console.log('setItem buttonPause completed'),
            );
            await AsyncStorage.setItem(
              'buttonStop',
              this.state.buttonStop.toString(),
              () => console.log('setItem buttonStop completed'),
            );
          } catch (error) {
            console.log('Error setitems local storage ', error);
          }
        } else {
          this.setState({isLoading: false});
          Alert.alert('Error al fichar');
        }
      });

      console.log('                                   ');
    } else {
      this.setState({isLoading: false});
      //Alert.alert('No tenemeos acceso a tu localización.');
      console.log(' Location permission not granted.');
    }
  }

  async stop() {
    this.setState({isLoading: true});

    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      Geolocation.getCurrentPosition(position => {
        this.setState({
          latitud: position.coords.latitude,
          longitud: position.coords.longitude,
        });
      });

      let data = {
        empleado_id: this.state.empleado_id,
        cliente_id: this.state.cliente_id,
        proyecto_id: this.state.proyecto_id,
        fecha: this.getYearMonthDay(),
        hora: this.getHour(),
        tipo_fichaje: 'Fin de Jornada',
        tiempoAcumulado: this.getFormattedTime(),
        observaciones: this.state.observaciones,
        longitud: this.state.longitud,
        latitud: this.state.latitud,
        isActive: this.state.isActive,
      };

      fetch(URL + 'insertFichaje', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
      }).then(async res => {
        let response = await res.json();
        if (response.success) {
          this.setState({isLoading: false});
          // Alert.alert(response.data.tipo_fichaje);
          this.resetStopwatch();
        } else {
          this.setState({isLoading: false});
          Alert.alert('Error al fichar');
        }
      });
    } else {
      this.setState({isLoading: false});
      // Alert.alert('No tenemeos acceso a tu localización.');
      console.log(' Location permission not granted.');
    }
  }

  render() {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Trabajado</Text>
        <Stopwatch
          start={this.state.isStopwatchStart}
          //To start
          reset={this.state.resetStopwatch}
          //To reset
          options={options}
          //options for the styling
          getTime={this.getFormattedTime.bind(this)}
        />
        <Text>Descansado</Text>
        <Stopwatch
          start={this.state.isStopwatchStart2}
          //To start
          reset={this.state.resetStopwatch2}
          //To reset
          options={options}
          //options for the styling
          getTime={this.getFormattedTime.bind(this)}
        />

        <View style={Styles.styles.bottom}>
          <TouchableHighlight
            disabled={this.state.buttonPlay}
            style={
              !this.state.buttonPlay
                ? Styles.options.toucbable
                : Styles.options.disabled
            }
            onPress={this.fichar.bind(this)}>
            {this.state.isLoading ? (
              <ActivityIndicator size="large" color="#0000ff" />
            ) : (
              <Icon name="play" size={25} color="white"></Icon>
            )}
          </TouchableHighlight>

          <TouchableHighlight
            disabled={this.state.buttonPause}
            style={
              !this.state.buttonPause
                ? Styles.options.toucbable
                : Styles.options.disabled
            }
            onPress={this.pausa.bind(this)}>
            {this.state.isLoading ? (
              <ActivityIndicator size="large" color="#0000ff" />
            ) : (
              <Icon name="pause" size={25} color="white"></Icon>
            )}
          </TouchableHighlight>

          <TouchableHighlight
            disabled={this.state.buttonStop}
            style={
              !this.state.buttonStop
                ? Styles.options.toucbableStop
                : Styles.options.disabled
            }
            onPress={this.stop.bind(this)}>
            {this.state.isLoading ? (
              <ActivityIndicator size="large" color="#0000ff" />
            ) : (
              <Icon name="stop" size={25} color="white"></Icon>
            )}
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}

const options = {
  container: {
    //backgroundColor: '#FF0000',
    padding: 5,
    borderRadius: 5,
    width: 200,
    alignItems: 'center',
  },
  text: {
    fontSize: 25,
    //  color: '#FFF',
    marginLeft: 7,
  },
};
