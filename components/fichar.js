export async function fichar() {
    this.setState({isLoading: true});

    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      Geolocation.getCurrentPosition(position => {
        this.setState({
          latitud: position.coords.latitude,
          longitud: position.coords.longitude,
        });
      });
      try {
        let empleado_id = await AsyncStorage.getItem('empleado_id', () =>
          console.log('getItem empleado_id completed'),
        );
        if (empleado_id !== null) {
          // We have data!!
          this.setState({empleado_id: empleado_id});
          console.log('From localStorage empleado_id:', empleado_id);
        } else {
          console.log('No hay resultado en localStrage empleado_id');
        }
      } catch (error) {
        // Error retrieving data
        console.log('Error getItem empleado_id localStorage:', error);
      }

      var dateObj = new Date();
      var month = dateObj.getUTCMonth() + 1;
      var day = dateObj.getUTCDate();
      var year = dateObj.getUTCFullYear();
      var newdate = year + '-' + month + '-' + day;

      var horayminuto = new Date();
      var hora = horayminuto.getHours();
      var minuto = horayminuto.getMinutes();
      var horaFinal = hora + ':' + minuto;

      let lastTipoFichaje;
      fetch(URL + 'lastUltimoFichaje?id=' + this.state.empleado_id, {
        method: 'GET',
        //body: {id: this.state.empleado_id},
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
      }).then(async res => {
        let response = await res.json();

        lastTipoFichaje = response.tipo_fichaje;
        console.log('lastTipoFichaje: ', lastTipoFichaje);

        if (
          (!this.state.isStopwatchStart &&
            lastTipoFichaje == 'Fin de Jornada') ||
          lastTipoFichaje == null
        ) {
          // empieza la jornada
          this.setState({
            tipo_fichaje: 'Inicio de Jornada',
          });

          console.log(' ', lastTipoFichaje, this.state.tipo_fichaje);
        } else {
          this.setState({tipo_fichaje: 'Fin de Pausa'});
        }

        let data = {
          empleado_id: this.state.empleado_id,
          cliente_id: this.state.cliente_id,
          proyecto_id: this.state.proyecto_id,
          fecha: newdate,
          hora: horaFinal,
          tipo_fichaje: this.state.tipo_fichaje,
          tiempoAcumulado: this.getFormattedTime(),
          observaciones: this.state.observaciones,
          longitud: this.state.longitud,
          latitud: this.state.latitud,
          isActive: this.state.isActive,
        };

        fetch(URL + 'insertFichaje', {
          method: 'POST',
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest',
          },
        }).then(async res => {
          let response = await res.json();
          if (response.success) {
            this.setState({isLoading: false});
            Alert.alert(response.data.tipo_fichaje);
          } else {
            this.setState({isLoading: false});
            Alert.alert('Error al fichar');
          }
        });

        this.setState({
          isStopwatchStart: !this.state.isStopwatchStart,
          resetStopwatch: false,
          isStopwatchStart2: false,

          buttonPlay: true,
          buttonPause: false,
          buttonStop: false,
        });

        console.log('                                   ');
      });
    } else {
      this.setState({isLoading: false});
      Alert.alert('No tenemeos acceso a tu localización.');
      console.log(' Location permission not granted.');
    }
  }