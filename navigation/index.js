import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Home from '../views/home';
import Login from '../views/login';
import Reservar from '../views/booking';
import Inicio from '../views/counter';
import Reservas from '../views/mySchedules';
import Timer from '../components/timer';
import Noti from '../views/push';

const MainNavigator = createStackNavigator(
  {
   // Noti: {screen: Noti},
   Login: {screen: Login},
   Timer: {screen: Timer},

   Reservar: {screen: Reservar},

    Home: {screen: Home},
    Reservas: {screen: Reservas},
   
    Inicio: {screen: Inicio},
  },
    {
    defaultNavigationOptions: {
      header: null,
    },
  }, 
);

const App = createAppContainer(MainNavigator);

export default App;
