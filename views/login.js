import React from 'react';
import {
  Button,
  Image,
  View,
  TextInput,
  AsyncStorage,
  ActivityIndicator,
  Alert,
  Switch,
  Text,
} from 'react-native';
import Styles from '../css/styles';
import {URL} from '../router/router.js';
import {PermissionsAndroid} from 'react-native';

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: 'caro@email.com',
      password: '1234',
      user: '',
      token: '',
      isLoading: false,
      rememberMe: 'false',
    };
  }

  async componentWillMount() {
    let recordarme = false;

    try {
      let rememberMe = await AsyncStorage.getItem('remeber', () =>
        console.log('getItem rememberMe completed'),
      );
      if (rememberMe !== null) {
        if (rememberMe !== 'false') {
          console.log('From localStorage rememberMe:', rememberMe);
          recordarme = true;
        }
      } else {
        console.log('No hay resultado en localStrage rememberMe');
      }
    } catch (error) {
      // Error retrieving data
      console.log('Error getItem password localStorage:', error);
    }

    if (recordarme) {
      try {
        let email = await AsyncStorage.getItem('email', () =>
          console.log('getItem email completed'),
        );
        if (email !== null) {
          // We have data!!
          this.setState({username: email.trim()});
          console.log('From localStorage:', email.replace(/^\s+|\s+$/g, ''));
        } else {
          console.log('No hay resultado en localStrage email');
        }
      } catch (error) {
        // Error retrieving data
        console.log('Error getItem email localStorage:', error);
      }

      try {
        let password = await AsyncStorage.getItem('password', () =>
          console.log('getItem password completed'),
        );
        if (password !== null) {
          // We have data!!
          this.setState({password: password.replace(/^\s+|\s+$/g, '')});
          console.log('From localStorage:', password);
        } else {
          console.log('No hay resultado en localStrage password');
        }
      } catch (error) {
        // Error retrieving data
        console.log('Error getItem password localStorage:', error);
      }

      this.onLogin();
    }
  }
  toggleRememberMe = value => {
    if (value) {
      this.setState({rememberMe: value});
    } else {
      this.setState({rememberMe: value});
    }
    console.log(this.state.rememberMe);
  };
  onLogin() {
    const {username, password} = this.state;

    this.setState({isLoading: true});
    //Alert.alert('Credentials', `${username} + ${password}`);

    let data = {
      email: username,
      password: password,
    };
    let url = URL + 'auth/login';
    fetch(url, {
      method: 'POST', // or 'PUT'
      body: JSON.stringify(data), // data can be `string` or {object}!
      headers: {
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
      },
    }).then(async res => {
      let response = await res.json();
      if (response.access_token) {
        console.log('Token: ', response.access_token, 'User:', response.user);
        this.setState({token: response.access_token});
        try {
          await AsyncStorage.setItem('token', response.access_token, () =>
            console.log('setItem token completed'),
          );
          await AsyncStorage.setItem('username', response.user, () =>
            console.log('setItem username completed'),
          );
          await AsyncStorage.setItem('empleado_id', response.empleado_id.toString(), () =>
          console.log('setItem empleado_id completed'),
        );
          await AsyncStorage.setItem('password', this.state.password, () =>
            console.log('setItem password completed'),
          );
          await AsyncStorage.setItem('email', this.state.username, () =>
            console.log('setItem email completed'),
          );
          await AsyncStorage.setItem('remeber', this.state.rememberMe.toString(), () =>
            console.log('setItem rememberMe completed'),
          );
        } catch (error) {
          console.log('Error local storage ', error);
        }

        try {
          let url = URL + 'user';
          fetch(url, {
            method: 'GET', // or 'PUT'
            headers: {
              'Content-Type': 'application/json',
              'X-Requested-With': 'XMLHttpRequest',
              Authorization: 'Bearer ' + this.state.token,
            },
          }).then(async res => {
            let response = await res.json();
            // console.log(response);
            this.setState({userid: response.id});
            this.props.navigation.navigate('Timer');

            try {
              console.log('userid: ', response.id);
              await AsyncStorage.setItem(
                'userid',
                JSON.stringify(response.id),
                () => console.log('setItem userid completed '),
              );

              this.setState({isLoading: false});
              
            } catch (error) {
              console.log('Error local storage token userid', error);
            }
          });
        } catch (error) {
          console.log('Error en el fetch /user', error);
        }
      } else {
        // Error login
        this.setState({isLoading: false});
        Alert.alert('Usuario/Contraseña no reconocidos');
        console.log(response.errors + response.message);
      }
    });
  }

  render() {
    //const {navigate} = this.props.navigation;
    if (this.state.isLoading) {
      return (
        <View style={Styles.styles.container}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      );
    } else {
      return (
        <View style={Styles.styles.container}>
          {/* <Text style={styles.titulo}>Reserva del coche</Text> */}
          <Image
            style={Styles.styles.logo}
            source={require('../src/logo.png')}
          />
          <TextInput
            value={this.state.username}
            onChangeText={username => this.setState({username})}
            placeholder={'Username'}
            style={Styles.styles.input}
          />
          <TextInput
            value={this.state.password}
            onChangeText={password => this.setState({password})}
            placeholder={'Password'}
            secureTextEntry={true}
            style={Styles.styles.input}
          />
          <View style={Styles.styles.switch}>
            <Switch
              value={this.state.rememberMe}
              onValueChange={value => this.toggleRememberMe(value)}
            />
            <Text>Recordarme</Text>
          </View>
          <Button
            title={'Login'}
            style={Styles.styles.input}
            onPress={this.onLogin.bind(this)}

            /*           onPress={() => this.props.navigation.navigate('Home')}
             */
          />
        </View>
      );
    }
  }
}
