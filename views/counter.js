import React from 'react';
import {
  Text,
  View,
  Alert,
  AsyncStorage,
  TouchableHighlight,
  ActivityIndicator,
  Dimensions,
} from 'react-native';
import Styles from '../css/styles';
import Geolocation from 'react-native-geolocation-service';
import {PermissionsAndroid} from 'react-native';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import {URL} from '../router/router.js';

const {width, height} = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 40.4674989;
const LONGITUDE = -3.8017149;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      user: '',
      userid: '',
      checkedIn: false,
      id_schedule: '',
      longitude: '',
      latitude: '',
      isLoading: false,
      hackHeight: height,
    };
  }

  async componentDidMount() {
    setTimeout(() => this.setState({hackHeight: height + 1}), 500);
    setTimeout(() => this.setState({hackHeight: height}), 1000);

    try {
      const username = await AsyncStorage.getItem('username', () =>
        console.log('getItem completed'),
      );
      if (username !== null) {
        // We have data!!S
        this.setState({user: username});
        console.log('From localStorage: ', username);
      } else {
        console.log('No hay resultado en localStrage username');
      }
    } catch (error) {
      // Error retrieving data
      console.log('Error getItem localStorage:', error);
    }

    try {
      const userid = await AsyncStorage.getItem('userid', () =>
        console.log('getItem completed'),
      );
      if (userid !== null) {
        // We have data!!
        this.setState({userid: userid});
        console.log('From localStorage: ', userid);
      } else {
        console.log('No hay resultado en localStrage userid');
      }
    } catch (error) {
      // Error retrieving data
      console.log('Error getItem localStorage:', error);
    }
  }

  isCheckIn() {
    if (this.state.checkedIn) {
      this.checkOut();
    } else {
      this.checkCarSchedule();
    }
  }

  async checkOut() {
    this.setState({isLoading: true});
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      Geolocation.getCurrentPosition(
        position => {
          let data = {
            id: this.state.id_schedule,
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          };
          let url = URL + 'checkOut';
          fetch(url, {
            method: 'POST', // or 'PUT'
            body: JSON.stringify(data), // data can be `string` or {object}!
            headers: {
              'Content-Type': 'application/json',
              'X-Requested-With': 'XMLHttpRequest',
            },
          }).then(async res => {
            let response = await res.json();
            console.log(response);
            // Alert.alert('Reserva terminada.');
            this.setState({checkedIn: false, isLoading: false});
          });
        },
        error => {
          // See error code charts below.
          console.log(error.code, error.message);
          this.setState({isLoading: false});
        },
        {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
      );
    } else {
      this.setState({isLoading: false});
      Alert.alert('No tenemeos acceso a tu localización.');
      console.log(' Location permission not granted.');
    }
  }

  async checkin() {
    this.setState({isLoading: true});
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      await Geolocation.getCurrentPosition(
        position => {
          console.log(position);
          this.setState({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          });
          let data = {
            id: this.state.id_schedule,
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          };
          let url = URL + 'checkIn';
          fetch(url, {
            method: 'POST', // or 'PUT'
            body: JSON.stringify(data), // data can be `string` or {object}!
            headers: {
              'Content-Type': 'application/json',
              'X-Requested-With': 'XMLHttpRequest',
            },
          }).then(async res => {
            let response = await res.json();
            console.log(response);
            //    Alert.alert('Reserva iniciada.');
            this.setState({checkedIn: true, isLoading: false});
          });
        },
        error => {
          // See error code charts below.
          console.log(error.code, error.message);
          this.setState({isLoading: false});
        },
        {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
      );
    } else {
      Alert.alert('No tenemeos acceso a tu localización.');

      console.log(' Location permission not granted.');
      this.setState({isLoading: false});
    }
    this.setState({isLoading: false});
  }
  checkCarSchedule() {
    let data = {user_id: this.state.userid};
    let url = URL + 'checkCarSchedule';
    fetch(url, {
      method: 'POST', // or 'PUT'
      body: JSON.stringify(data), // data can be `string` or {object}!
      headers: {
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
      },
    }).then(async res => {
      let response = await res.json();
      console.log(response);
      if (response.success === true) {
        this.setState({
          id_schedule: response.id_schedule,
        });
        this.checkin();
      } else {
        Alert.alert('No tienes niguna reserva en este momento.');
      }
    });
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={Styles.styles.container}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      );
    } else {
      return (
        <View style={{flex: 1}}>
          <View style={{paddingBottom: this.state.hackHeight}}>
            <MapView
              provider={PROVIDER_GOOGLE}
              followsUserLocation={true}
              showsUserLocation={true}
              style={Styles.styles.map}
              initialRegion={{
                latitude: LATITUDE,
                longitude: LONGITUDE,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
              }}></MapView>
          </View>
          <TouchableHighlight
            onPress={this.isCheckIn.bind(this)}
            style={
              !this.state.checkedIn
                ? Styles.options.toucbable
                : Styles.options.toucbableStop
            }
            underlayColor="#a1a61a">
            <Text>{!this.state.checkedIn ? 'START' : 'STOP'}</Text>
          </TouchableHighlight>
        </View>
      );
    }
  }
}
App.propTypes = {
  provider: MapView.ProviderPropType,
};
