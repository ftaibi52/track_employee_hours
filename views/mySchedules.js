import React from 'react';
import {
  Text,
  View,
  ScrollView,
  SafeAreaView,
  AsyncStorage,
  FlatList,
  RefreshControl,
} from 'react-native';
import Styles from '../css/styles';
import {URL} from '../router/router.js';

function wait(timeout) {
  return new Promise(resolve => {
    setTimeout(resolve, timeout);
  });
}

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      token: '',
      user: '',
      userid: '',
      reservas: '',
      fecha_recogida: '',
      fecha_entrega: '',
      latitude_ini: '',
      longitude_ini: '',
      refreshing: false,
    };
  }


  async componentDidMount() {
    try {
      const username = await AsyncStorage.getItem('username', () =>
        console.log('getItem completed'),
      );
      if (username !== null) {
        // We have data!!
        this.setState({user: username});
        console.log('From localStorage: ', username);
      } else {
        console.log('No hay resultado en localStrage username');
      }
    } catch (error) {
      // Error retrieving data
      console.log('Error getItem localStorage:', error);
    }

    try {
      const userid = await AsyncStorage.getItem('userid', () =>
        console.log('getItem completed'),
      );
      if (userid !== null) {
        // We have data!!
        this.setState({userid: userid});
        console.log('From localStorage: ', userid);
      } else {
        console.log('No hay resultado en localStrage userid');
      }
    } catch (error) {
      // Error retrieving data
      console.log('Error getItem localStorage:', error);
    }
    this.getSchedule();
  }

  getSchedule() {
    console.log('Actualizando nuevas reservas', this.state.userid);
    let url = URL + 'getAllSchedules' + '?user_id=' + this.state.userid;
    fetch(url, {
      method: 'POST', // or 'PUT'
      headers: {
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
      },
    }).then(async res => {
      let response = await res.json();
      this.setState({
        reservas: response.carSchedules,
      });
      //  console.log(response.carSchedules);
    });
  }

  estado(status) {
    console.log(status);

    if (status === 1) {
      return 'En uso';
    } else if (status === 2) {
      return 'Terminada';
    } else if (status === 4) {
      return 'Desactivada';
    } else if (status === 0) {
      return 'Pendiente';
    }
  }

  onRefresh = () => {
    this.setState({refreshing: true});
    this.getSchedule();
    // In actual case set refreshing to false when whatever is being refreshed is done!
    setTimeout(() => {
      this.setState({refreshing: false});
    }, 2000);
  };
  render() {
    let res = this.state.reservas;
    let data;
    let resBool;
    if (res !== undefined) {
      data = Object.keys(res).map(key => {
        return res[key];
        console.log('-----------', res[key]);
      });
      resBool = false;
    } else {
      console.log('Data esta vacion', data);
      resBool = true;
    }

    return (
      <View>
        <Text style={Styles.styles.nombre}>
          Hola {this.state.user}, estas son tus reservas:
        </Text>

        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.onRefresh}
              title="Pull to refresh"
              nestedScrollEnabled="true"
            />
          }>
          <Text style={Styles.styles.noReservas}>
            {resBool ? 'No tienes reservas.' : ''}
          </Text>
          <Text></Text>
          <View>
            <FlatList
              data={data}
              renderItem={({item}) => (
                <View style={Styles.styles.textContainer}>
                  <View style={Styles.styles.leftContainer}>
                    <Text style={Styles.styles.rowLabelText}>
                      {item.fecha_reserva_ini}
                    </Text>
                  </View>

                  <View style={Styles.styles.rightContainer}>
                    <Text style={Styles.styles.rowLabelText}>
                      {this.estado(item.status)}
                    </Text>
                  </View>
                </View>
              )}
            />
          </View>
        </ScrollView>

        <View style={Styles.styles.form}></View>
      </View>
    );
  }
}
